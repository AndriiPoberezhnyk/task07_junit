package com.epam.training.model.plateau;

import java.util.*;

public class LongestSequence implements PlateauModel {
    private Plateau plateau;
    private int length;
    private List<Integer> startIndexes;

    public LongestSequence() {
        this.plateau = new Plateau();
        length = 0;
        startIndexes = new ArrayList<>();
        findLongestSequences();
    }

    @Override
    public Integer[] getPlateau() {
        return plateau.getArray();
    }

    @Override
    public int getLongestSequenceLength() {
        return length;
    }

    @Override
    public List<Integer> getLongestSequenceStartIndexes() {
        return startIndexes;
    }

    private void findLongestSequences() {
        Integer[] integers = getPlateau();
        Map<Integer, Integer> allSequences = new LinkedHashMap<>();
        allSequences = checkNext(integers, 0, new ArrayList<Integer>(),
                allSequences);
        Optional<Integer> optionalLength =
                allSequences.values().stream().reduce(Math::max);
        optionalLength.ifPresent(integer -> length = integer);
        allSequences.entrySet().stream()
                .filter(entry -> entry.getValue() == length)
                .forEach(entry -> startIndexes.add(entry.getKey()));
    }

    private Map<Integer, Integer> checkNext(Integer[] integers,
                                            int current,
                                            List<Integer> sequence,
                                            Map<Integer, Integer> allSequences) {
        if (current == integers.length - 1) {
            return allSequences;
        }
        Integer currentValue = integers[current];
        Integer nextValue = integers[current + 1];
        if (nextValue > currentValue) {
            sequence.clear();
            sequence.add(nextValue);
            allSequences = checkNext(integers, ++current, sequence,
                    allSequences);
        } else if (nextValue.equals(currentValue) && sequence.size() > 0) {
            sequence.add(nextValue);
            allSequences = checkNext(integers, ++current, sequence,
                    allSequences);
        } else if (sequence.size() > 0) {
            allSequences.put(current + 1 - sequence.size(), sequence.size());
            sequence.clear();
            allSequences = checkNext(integers, ++current, sequence,
                    allSequences);
        } else {
            allSequences = checkNext(integers, ++current, sequence,
                    allSequences);
        }
        return allSequences;
    }
}