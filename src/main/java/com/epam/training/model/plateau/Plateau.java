package com.epam.training.model.plateau;

import java.util.Random;
import java.util.ResourceBundle;

class Plateau {
    private ResourceBundle bundle;
    private Integer[] array;

    Plateau() {
        bundle = ResourceBundle.getBundle("const");
        generateNewPlateau();
    }

    private void generateNewPlateau(){
        int length = Integer.parseInt(bundle.getString("PlateauSize"));
        int bound = Integer.parseInt(bundle.getString("BoundValue"));
        array = new Integer[length];
        for (int i = 0; i < array.length; i++) {
            array[i] = 1 + new Random().nextInt(bound);
        }
    }

    Integer[] getArray(){
        return array;
    }

}
