package com.epam.training.model.plateau;

import java.util.List;

public interface PlateauModel {
    Integer[] getPlateau();
    int getLongestSequenceLength();
    List<Integer> getLongestSequenceStartIndexes();

}

