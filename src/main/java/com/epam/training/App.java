package com.epam.training;

import com.epam.training.view.View;

public class App {
    public static void main(String[] args) {
        new View().run();
    }
}