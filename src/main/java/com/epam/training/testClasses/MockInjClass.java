package com.epam.training.testClasses;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class MockInjClass implements MockInjInterface{
    private static final Logger logger =
            LogManager.getLogger(MockInjClass.class.getName());
    @Override
    public String getValue(int a) {
        return Integer.toString(a);
    }
}
