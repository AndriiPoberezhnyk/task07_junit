package com.epam.training.testClasses;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class TestClass {
    private static final Logger logger =
            LogManager.getLogger(TestClass.class.getName());

    public TestClass() {
    }

    public boolean aBoolean(int a){
        return a > 0;
    }

    public String convert(Integer a){
        return Integer.toString(a);
    }

    public int add(int a, int b){
        return a+b;
    }

    public void voidMethod(){
        logger.info("Test void method");
    }

    public int getIntFromMockClassPlusOne(int a){
        MockInjInterface mockInjClass = new MockInjClass();
        return Integer.parseInt(mockInjClass.getValue(a)) + 1;
    }

}