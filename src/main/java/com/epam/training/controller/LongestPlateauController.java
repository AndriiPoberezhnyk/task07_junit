package com.epam.training.controller;

import com.epam.training.model.plateau.LongestSequence;
import com.epam.training.model.plateau.PlateauModel;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class LongestPlateauController implements LongestPlateau{
    private PlateauModel plateauModel;
    private int length;
    private List<Integer> startIndexes;

    public LongestPlateauController() {
        plateauModel = new LongestSequence();
        length = plateauModel.getLongestSequenceLength();
        startIndexes = plateauModel.getLongestSequenceStartIndexes();
    }

    @Override
    public List<Integer[]> getLongestSequences() {
        List<Integer[]> longestSequences = new ArrayList<>();
        Integer[] plateauArray = plateauModel.getPlateau();
        startIndexes.forEach(integer -> longestSequences
                .add(Arrays.copyOfRange(plateauArray, integer-1,
                        integer+length+1)));
        return longestSequences;
    }

    @Override
    public Integer[] getPlateau() {
        return plateauModel.getPlateau();
    }

    @Override
    public int getLength() {
        return length;
    }

    @Override
    public List<Integer> getStartIndexes() {
        return startIndexes;
    }
}