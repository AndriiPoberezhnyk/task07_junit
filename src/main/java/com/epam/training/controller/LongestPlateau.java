package com.epam.training.controller;

import java.util.List;

public interface LongestPlateau {
    List<Integer[]> getLongestSequences();
    int getLength();
    List<Integer> getStartIndexes();
    Integer[] getPlateau();
}