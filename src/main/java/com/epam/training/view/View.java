package com.epam.training.view;

import com.epam.training.controller.LongestPlateau;
import com.epam.training.controller.LongestPlateauController;
import com.epam.training.controller.Minesweeper;
import com.epam.training.controller.MinesweeperController;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

public class View {
    private static final Logger logger = LogManager.getLogger(View.class.getName());
    private Map<String, String> menu;
    private Map<String, Runnable> methodsMenu;
    private Scanner scanner;
    private Minesweeper minesweeperController;
    private Locale locale;
    private ResourceBundle bundle;

    public View() {
        locale = new Locale("uk");
        bundle = ResourceBundle.getBundle("Menu", locale);
        scanner = new Scanner(System.in);
        menu = new LinkedHashMap<>();
        methodsMenu = new LinkedHashMap<>();
        minesweeperController = new MinesweeperController();

    }

    public void run(){
        String keyMenu;
        do {
            generateMainMenu();
            keyMenu = scanner.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).run();
            } catch (Exception ignored) {
            }
        } while (!keyMenu.equals("Q"));
    }

    private void generateMainMenu() {
        menu.clear();
        menu.put("1", bundle.getString("1"));
        menu.put("2", bundle.getString("2"));
        menu.put("3", bundle.getString("3"));
        menu.put("4", bundle.getString("4"));
        menu.put("Q", bundle.getString("Q"));

        methodsMenu.clear();
        methodsMenu.put("1", this::i18nEnglish);
        methodsMenu.put("2", this::i18nUkrainian);
        methodsMenu.put("3", this::plateauMethod);
        methodsMenu.put("4", this::startMinesweeper);
        outputMenu();
    }

    private void i18nEnglish() {
        locale = new Locale("en");
        bundle = ResourceBundle.getBundle("Menu");
    }

    private void i18nUkrainian() {
        locale = new Locale("uk");
        bundle = ResourceBundle.getBundle("Menu", locale);
    }

    private void plateauMethod(){
        LongestPlateau longestPlateauController = new LongestPlateauController();
        List<Integer> startsFrom = longestPlateauController.getStartIndexes();
        logger.info(Arrays.toString(longestPlateauController.getPlateau()));
        logger.info("Longest sequence(s) has length "
                + longestPlateauController.getLength());
        logger.info("Sequences found = "+
                longestPlateauController.getLongestSequences().size());
        for (int i = 0; i < startsFrom.size(); i++) {
            logger.info("Sequence starts at index = " + startsFrom.get(i)
                    + ", sequence -> "
                    + Arrays.toString(longestPlateauController
                    .getLongestSequences().get(i)));
        }
    }

    private void startMinesweeper(){

    }

    private void outputMenu() {
        logger.trace("MENU:");
        for (String option : menu.values()) {
            logger.trace(option);
        }
        logger.trace(">>> ");
    }

}