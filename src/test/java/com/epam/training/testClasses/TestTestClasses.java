package com.epam.training.testClasses;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Mock;
import java.util.stream.Stream;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class TestTestClasses {
    private static final Logger logger =
            LogManager.getLogger(TestTestClasses.class.getName());

    @Mock
    MockInjInterface mockInjInterface = mock(MockInjClass.class);

    @BeforeAll
    static void initBeforeAll(){
        logger.info("Test before all methods only once");
    }

    @DisplayName("Should pass true")
    @ParameterizedTest(name = "{index} => message=''{0}''")
    @ValueSource(ints = {1, 10, Integer.MAX_VALUE})
    void testABooleanForTrue(int a){
        TestClass test = new TestClass();
        boolean value = test.aBoolean(a);
        assertTrue(value, "result " + value +";");
    }

    @DisplayName("Should pass false")
    @ParameterizedTest(name = "{index} => message=''{0}''")
    @ValueSource(ints = {0, -1, Integer.MIN_VALUE})
    void testABooleanForFalse(int a){
        TestClass test = new TestClass();
        boolean value = test.aBoolean(a);
        assertFalse(value, "result " + value +";");
    }

    @DisplayName("Test string convert")
    @ParameterizedTest(name = "{index} => message=''{0}''")
    @ValueSource(ints = {0, 1, -5})
    void testConvertForEquals(Integer a){
        TestClass test = new TestClass();
        String value = test.convert(a);
        assertEquals(value, Integer.toString(a), "result " + value + ";");
    }

    @DisplayName("Test string convert for NPE")
    @ParameterizedTest(name = "{index} => message=''{0}''")
    @MethodSource("integerTestStream")
    void testConvertThrows(Integer a){
        TestClass test = new TestClass();
        assertThrows(NullPointerException.class, ()->test.convert(a),
                "Doesn't throw exception");
    }

    @DisplayName("Test string convert for NPE")
    @RepeatedTest(100)
    void testConvertThrows(){
        TestClass test = new TestClass();
        assertThrows(NullPointerException.class, ()->test.convert(null),
                "Doesn't throw exception");
    }

    @DisplayName("Test mockInj")
    @Test
    void testGetStringFromMock(){
        when(mockInjInterface.getValue(5)).thenReturn("5");
        TestClass test = new TestClass();
        int value = Integer.parseInt(mockInjInterface.getValue(5)) + 1;
        assertEquals(value,6, "wrong return");
        verify(mockInjInterface).getValue(5);
    }

    private static Stream<Object> integerTestStream() {
        return Stream.of(1,-1, 0, Integer.MAX_VALUE, null);
    }
}
