package com.epam.training.model.plateau;

import com.epam.training.controller.LongestPlateau;
import com.epam.training.controller.LongestPlateauController;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TestLongestPlateauController {

    @DisplayName("Test number of sequences same as number of start index")
    @Test
    void testGetLongestSequencesForNumberOfSeq(){
        LongestPlateau longestPlateau = new LongestPlateauController();
        int amount = longestPlateau.getStartIndexes().size();
        assertEquals(longestPlateau.getLongestSequences().size(), amount,
                "different sizes");
    }
}
