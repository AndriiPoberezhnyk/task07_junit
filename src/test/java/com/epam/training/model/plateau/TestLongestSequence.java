package com.epam.training.model.plateau;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import java.util.HashSet;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class TestLongestSequence {

    @RepeatedTest(100)
    void testGetLength(){
        PlateauModel plateauModel = new LongestSequence();
        assertTrue(plateauModel.getLongestSequenceLength() > 0, "wrong length" +
                " if sequence not found");
    }

    @DisplayName("True if return NotNull")
    @RepeatedTest(50)
    void testGetArrayForReturn(){
        PlateauModel plateauModel = new LongestSequence();
        assertNotNull(plateauModel.getPlateau());
    }

    @DisplayName("True if return Integer[]")
    @RepeatedTest(50)
    void testGetArrayForReturnType(){
        PlateauModel plateauModel = new LongestSequence();
        assertTrue(plateauModel.getPlateau() instanceof Integer[]);
    }

    @DisplayName("True if sequence length less or equal array size")
    @RepeatedTest(100)
    void testGetLongestSequenceLength(){
        PlateauModel plateauModel = new LongestSequence();
        int length = Integer.parseInt(ResourceBundle.getBundle("const").getString(
                "PlateauSize"));
        assertTrue(plateauModel.getLongestSequenceLength() <= length
                        && plateauModel.getLongestSequenceLength() >= 0,
                "wrong sequence length");
    }

    @DisplayName("Test for unique start indexes")
    @RepeatedTest(100)
    void testGetStartIndexes(){
        PlateauModel plateauModel = new LongestSequence();
        Set<Integer> indexes =
                new HashSet<>(plateauModel.getLongestSequenceStartIndexes());
        assertEquals(indexes.size(), plateauModel.getLongestSequenceStartIndexes().size(), "exists same indexes");
    }

    @DisplayName("Test for start indexes > 0 & < plateau size")
    @RepeatedTest(100)
    void testGetStartIndexesBounds(){
        PlateauModel plateauModel = new LongestSequence();
        int length =
                Integer.parseInt(ResourceBundle.getBundle("const").getString(
                        "PlateauSize"));
        List<Integer> indexes = plateauModel.getLongestSequenceStartIndexes();
        indexes.forEach(index -> assertTrue(index>0 && index< length, "index " +
                "=" + index));
    }

}