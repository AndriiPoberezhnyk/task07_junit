package com.epam.training.model.plateau;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;

import java.util.ResourceBundle;
import static org.junit.jupiter.api.Assertions.*;

class TestPlateau {

    @DisplayName("True if size same as in props")
    @RepeatedTest(100)
    void testGetArrayForSize(){
        Plateau plateau = new Plateau();
        int length = Integer.parseInt(ResourceBundle.getBundle("const").getString(
                "PlateauSize"));
        assertEquals(plateau.getArray().length, length, "different lengths");
    }

    @DisplayName("True if values in bounds from props")
    @Test
    void testGetArrayForValues(){
        Plateau plateau = new Plateau();
        int bound =
                Integer.parseInt(ResourceBundle.getBundle("const").getString(
                        "BoundValue"));
        Integer[] values = plateau.getArray();
        for (Integer in: values) {
            assertTrue(in>=1 && in<=bound, "different lengths");
        }
    }

    @DisplayName("True if values NotNull")
    @Test
    void testGetArrayForNotNullValues(){
        Plateau plateau = new Plateau();
        Integer[] values = plateau.getArray();
        for (Integer in: values) {
            assertNotNull(in, "found null");
        }
    }

    @DisplayName("True if return NotNull")
    @RepeatedTest(50)
    void testGetArrayForReturn(){
        Plateau plateau = new Plateau();
        assertNotNull(plateau.getArray());
    }

    @DisplayName("True if return Integer[]")
    @RepeatedTest(50)
    void testGetArrayForReturnType(){
        Plateau plateau = new Plateau();
        assertTrue(plateau.getArray() instanceof Integer[]);
    }
}